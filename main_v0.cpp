////////////////////////////////////////////////////////////////////////
// OOP Tutorial : Operator Overloading (version 0) 
////////////////////////////////////////////////////////////////////////

//--------include libraries
#include <iostream>	//for cin, cout variables and << and >> operators
#include <string>	//for string
using namespace std;

//--------include Time class declarations
#include "Time.h"

//--------start program
int main()
{
	Time t1;
	t1.setTime(12, 34, 56);
	cout << "\n t1 hours are " << t1.getHours();
	cout << "\n t1 minutes are " << t1.getMinutes();
	cout << "\n t1 seconds are " << t1.getSeconds();
	cout << "\n t1 is ";
	t1.printTime();

	t1.setTime( "12:34:57");
	cout << "\n t1 is ";
	t1.printTime();

	if ( t1.isValid())
		cout << "\n t1 is valid";
	else
		cout << "\n t1 is NOT valid";

	Time t2;
	t2.setTimeFromSystem();
	cout << "\n t2 is ";
	t2.printTime();

	Time t3;
	cout << "\n Enter values for t3 (HH:MM:SS) ";
	t3.setTimeFromUserInput();
	cout << "\n t3 is ";
	t3.printTime();
	if ( t3.isValid())
		cout << "\n t3 is valid";
	else
		cout << "\n t3 is invalid";

	if (t2.isSameAs(t3))
		cout << "\n t2 and t3 are identical";
	else
		cout << "\n t2 and t3 are NOT identical";

	cout << "\n t2.isEarlierThan( t3) is ";
	if ( t2.isEarlierThan( t3))
		cout << "true";
	else
		cout << "false";
	cout << "\n t2.isLaterThan( t3) is ";
	if ( t2.isLaterThan( t3))
		cout << "true";
	else
		cout << "false";

	Time t4(300); 	//create time from long value
	cout << "\n t4 (from long 300 using toFormattedString()) is ";
	string s( t4.toFormattedString());
	cout << s;

	Time t5("00:10:10"); 	//create time from string value
	cout << "\n t5 (from string \"00:10:10\" using toLong()) is ";
	long l( t5.toSeconds());
	cout << l;

	cout << "\n\n";
	system("pause");
	return 0;
}
