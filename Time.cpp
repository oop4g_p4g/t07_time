// Time.cpp: implementation of the Time class
//////////////////////////////////////////////////////////////////////
#include "Time.h"

Time::Time()
:	hours_(0), minutes_(0), seconds_(0)
{}
Time::Time(const Time& t)
:	hours_(t.hours_), minutes_(t.minutes_), seconds_(t.seconds_)
{
	assert(t.isValid());
}
Time::Time(int h, int m, int s) 
:	hours_(h), minutes_(m), seconds_(s)
{
	assert((h >= 0) && (h < 24) && (m >= 0) && (m < 60) && (s >= 0) && (s < 60));
}
Time::Time(long lt)
:	hours_(((lt / 60) / 60) % 24), minutes_((lt / 60) % 60), seconds_(lt % 60)
{
	assert (lt >= 0);
}
Time::Time(const string& strt) {
	istringstream is_time(strt);
	char ch;			//(any) colon field delimiter
	is_time >> hours_ >> ch >> minutes_ >> ch >> seconds_;
}
Time::~Time() {
}

int Time::getHours() const {
	return hours_;
}
int Time::getMinutes() const {
	return minutes_;
}
int Time::getSeconds() const {
	return seconds_;
}
void Time::setTime(int h, int m, int s) {
//pre: h:m:t is a valid time
	Time(h, m, s).isValid(); 
	hours_ = h;
	minutes_ = m;
	seconds_ = s;
}
void Time::setTime(long secs) {
//pre: secs >= 0 and represents a valid time
	assert(secs >= 0);
	Time temp(((secs / 60) / 60) % 24, (secs / 60) % 60, secs % 60);
	assert(temp.isValid());
	hours_ = temp.hours_;
	minutes_ = temp.minutes_;
	seconds_ = temp.seconds_;
}
void Time::setTime(const string& strt) {
//pre: str represents a valid time
	istringstream is_time(strt);
	char ch;			//(any) colon field delimiter
	is_time >> hours_ >> ch >> minutes_ >> ch >> seconds_;
}
void Time::setTimeFromSystem() {
	//set to system time
	time_t now = time(0);
	struct tm t;
	localtime_s(&t, &now);
	hours_ = t.tm_hour;
	minutes_ = t.tm_min;
	seconds_ = t.tm_sec;
}
void Time::setTimeFromUserInput() {
//read in a formatted digital time (HH:MM:SS) - assume user enter valid data
	char ch;	//(any) colon field delimiter
	cin >> hours_ >> ch >> minutes_ >> ch >> seconds_;
}
void Time::printTime() const {
//print out formatted digital time (HH:MM:SS)
	const char prev(cout.fill ('0'));
	cout << setfill('0');
	cout << setw(2) << hours_ << ":";
	cout << setw(2) << minutes_ << ":";
	cout << setw(2) << seconds_;
	cout.fill(prev);
}
bool Time::isValid() const {
	return (hours_ >= 0) && (hours_ < 24) &&
	       (minutes_ >= 0) && (minutes_ < 60) &&
	       (seconds_ >= 0) && (seconds_ < 60);
}
bool Time::isSameAs(const Time& t) const {
//pre: t is a valid time
	assert(t.isValid());
	return (hours_ == t.hours_) &&
	       (minutes_ == t.minutes_) &&
	       (seconds_ == t.seconds_);
}
bool Time::isDifferentFrom(const Time& t) const {
//pre: t is a valid time
	assert(t.isValid());
	return (! (isSameAs(t)));		//same as return (! (this->isSameAs(t)));
}
bool Time::isEarlierThan(const Time& t) const {
//pre: t is a valid time
	assert(t.isValid());
	bool strictlyEarlier;
	if (hours_ < t.hours_)
		strictlyEarlier = true;
	else
		if ((hours_ == t.hours_) && (minutes_ < t.minutes_))
			strictlyEarlier = true;
		else
			if ((hours_ == t.hours_) &&
			    (minutes_ == t.minutes_) &&
			    (seconds_ < t.seconds_))
				strictlyEarlier = true;
			else
				strictlyEarlier = false;
	return strictlyEarlier;
}
bool Time::isLaterThan(const Time& t) const {
//pre: t is a valid time
	assert(t.isValid());
	return t.isEarlierThan(*this);
}
string Time::toFormattedString() const {
	ostringstream os_time;
	os_time << setfill('0');
	os_time << setw(2) << hours_ << ":";
	os_time << setw(2) << minutes_ << ":";
	os_time << setw(2) << seconds_;
	return os_time.str();
}
long Time::toSeconds() const {  	//return time in long secs
	return (hours_ * 3600) + (minutes_ * 60) + seconds_;
}