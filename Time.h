// Time.h: interface for the Time class
//////////////////////////////////////////////////////////////////////

#if !defined(TimeH)
#define TimeH

#include <cassert>	//for assert
#include <iostream>	//for cin >>, cout << and random number routines
#include <iomanip>	//for setw and setfill
#include <ctime>	//for time used in random number routines and time_t, struct tm, time, localtime
#include <string>	//for string
#include <sstream>	//for string stream 
using namespace std;

class Time {
public:
	//constructors & destructor
	Time();				//default constructor
	Time( const Time&);	//copy constructor
	Time( int h, int m, int s);	//simple constructor
	explicit Time( long);			//conversion constructor
	explicit Time( const string&);	//conversion constructor
	~Time();		//destructor
	//public functions
	//query times
	int getHours() const;	//return data member value, hours_
	int getMinutes() const;	//return data member value, minutes_
	int getSeconds() const;	//return data member value, seconds_
	void printTime() const;	//print out formatted digital time (as HH:MM:SS)
	bool isValid() const; 	//check if is valid
	//compare times
	bool isSameAs( const Time& t) const; 	//check if same as t
	bool isDifferentFrom( const Time& t) const; 	//check if not same as t
	bool isEarlierThan( const Time& t) const;	//check strictly earlier than t
	bool isLaterThan( const Time& t) const;	//check strictly later than t
	//type conversion
	string toFormattedString() const;	//return formatted string ("HH:MM:SS")
	long toSeconds() const;   	//return time in long secs
	//update time
	void setTime( int h, int m, int s); 	//set to given values
	void setTime( long lt); 	//set time from long value
	void setTime( const string& strt); 	//set time from string value ("HH:MM:SS")
	void setTimeFromSystem(); 	//set to system time
	void setTimeFromUserInput();	//read in formatted digital time (as HH:MM:SS)

private:
	//private data members
	int hours_, minutes_, seconds_;
};

#endif // !defined(TimeH)
